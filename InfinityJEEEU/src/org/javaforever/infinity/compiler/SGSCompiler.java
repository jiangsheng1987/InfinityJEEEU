package org.javaforever.infinity.compiler;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;

import org.javaforever.infinity.domain.Domain;
import org.javaforever.infinity.domain.Dropdown;
import org.javaforever.infinity.domain.Field;
import org.javaforever.infinity.domain.Prism;
import org.javaforever.infinity.domain.Project;
import org.javaforever.infinity.domain.Type;
import org.javaforever.infinity.domain.ValidateInfo;
import org.javaforever.infinity.exception.ValidateException;
import org.javaforever.infinity.generator.DBDefinitionGenerator;
import org.javaforever.infinity.generator.MysqlDBDefinitionGenerator;
import org.javaforever.infinity.utils.DomainUtil;
import org.javaforever.infinity.utils.StringUtil;

public class SGSCompiler {
	protected final static String [] forbiddenwords = {"abstract", "assert","boolean", "break", "byte", "case", "catch", "char", "class", "const", "continue", "default", "do", "double", "else", "enum","extends", "final", "finally", "float", "for", "if",

	                                      "implements", "import", "instanceof", "int", "interface", "long", "native", "new", "package", "private", "protected", "public", "return", "short", "static", "strictfp", "super","switch",

	                                      "synchronized", "this", "throw", "throws","transient", "try", "void", "volatile", "while","byValue", "cast", "false", "future", "generic", "inner", "operator", "outer", "rest", "true", "var","goto","const","null"};
	
	protected final static String [] sqlKeyWords = {"alter","and","as","asc","between","by","count","create","delete","desc","distinct","drop","from","group","having","in","insert","into","is","join","like","not","on","or","order","select","set","table","union","update","values","where","limit",
			"bool","boolean","bit","blob","enum","long","longblob","longtext","medium","mediumblob","mediumint","mediumtext","time","timestamp","tinyblob","tinyint","tinytext","text","bigint","int","int1","int2","int3","int4","int8","integer","float","float4","float8","double","char","varbinary","varchar","varcharacter","precision","real","date","datetime","year","unsigned","signed","decimal","numeric",
			"false","true","null","unknown","date","time","timestamp"};
	
	public static boolean isSqlKeyword(String notion){
		for (String word: sqlKeyWords){
			if (word.equals(notion)) return true;
		}
		return false;
	}
	
	public static boolean isForbidden(String notion){
		for (String word: forbiddenwords){
			if (word.equals(notion)) return true;
		}
		return false;
	}
 
	public SGSCompiler() {}
	public static Project translate(String sgs,boolean ignoreWarning) throws ValidateException, Exception {
		try {
			if (containsRoundAndSquareBrackets(sgs)){
				throw new ValidateException("源码中存在圆括号或方括号。");
			}
			List<String> notions = parseNotions(sgs);
			Project project = notionsToProject(notions);
			ValidateInfo info = basicValidateProject(project);
			project.setSgsSource(sgs);
			if (ignoreWarning|| info.getCompileWarnings().size()==0){
				return project;
			} else {
				ValidateException em = new ValidateException(info);
				throw em;
			}
		} catch (ValidateException e){
			ValidateInfo info = e.getValidateInfo();
			for (String s: info.getCompileErrors()){
				System.out.println(s);
			}
			for (String s: info.getCompileWarnings()){
				System.out.println(s);
			}
			throw e;
		}
	}
	
	public static List<String> parseNotions(String sgs){
		return SGSTokenizer.generateTokens(sgs);
	}
	
	public static Project notionsToProject(List<String> notions) throws ValidateException, Exception {
		Project project;
		boolean headFinish = false;
		Stack<String> projectStack = new Stack<String>();
		DBDefinitionGenerator dbdg = new MysqlDBDefinitionGenerator();
		for (int i=0; i < notions.size();i++) {
			if ("project".equals(notions.get(i))){
				project = new Project();
				if (!isKeyword(notions.get(i+1))){
					project.setStandardName(notions.get(i+1));
					project.setDbName(project.getStandardName());
					project.setTechnicalstack("jeeeu");
					dbdg.setDbName(notions.get(i+1));
					if ("{".equals(notions.get(i+2))){
						projectStack.push("{");
						for (int j=i+3; j < notions.size();j++) {

						if ("packagetoken".equals(notions.get(j))){
							if (":".equals(notions.get(j+1))&&!isKeyword(notions.get(j+2))) project.setPackageToken(notions.get(j+2));
							if (";".equals(notions.get(j+3))) j += 4;
						}
						
						if ("dbprefix".equals(notions.get(j))){
							if (":".equals(notions.get(j+1))&&!isKeyword(notions.get(j+2))) project.setDbPrefix(notions.get(j+2));
							if (";".equals(notions.get(j+3))) j += 4;
						}
						
						if ("dbusername".equals(notions.get(j))){
							if (":".equals(notions.get(j+1))&&!isKeyword(notions.get(j+2))) project.setDbUsername(notions.get(j+2));
							if (";".equals(notions.get(j+3))) j += 4;
						}
						
						if ("dbpassword".equals(notions.get(j))){
							if (":".equals(notions.get(j+1))&&!isKeyword(notions.get(j+2))) project.setDbPassword(notions.get(j+2));
							if (";".equals(notions.get(j+3))) j += 4;
						}
						

						if ("dbname".equals(notions.get(j))){
							if (":".equals(notions.get(j+1))&&!isKeyword(notions.get(j+2))) {
								project.setDbName(notions.get(j+2));
								dbdg.setDbName(notions.get(j+2));
							}
							if (";".equals(notions.get(j+3))) j += 4;
						}
						
						
						if ("emptypassword".equals(notions.get(j))){
							project.setEmptypassword(true);
							if (";".equals(notions.get(j+1))) j += 2;
						}
						
						if ("technicalstack".equals(notions.get(j))){
							if (":".equals(notions.get(j+1))&&!isKeyword(notions.get(j+2))) {
								String rawTech = notions.get(j+2);
								String formatedTech = "jeeeu";
								if (rawTech == null || rawTech.equalsIgnoreCase("") || rawTech.equalsIgnoreCase("jeeeu") ){
									formatedTech = "jeeeu";
								}
								project.setTechnicalstack(formatedTech);
							}
							if (";".equals(notions.get(j+3))) j += 4;
						}

						
						if ("domain".equals(notions.get(j))|| "datadomain".equals(notions.get(j))|| "prism".equals(notions.get(j))){
							headFinish = true;
						}
						if (headFinish){	
							List<String> subNotions = notions.subList(j, notions.size());
							Stack<String> emptyStack = new Stack<>();
							List<String> domainNotions = fixDomainsNotions(notions.subList(j, notions.size()),projectStack);
							System.out.println("=================JerryDebug==================");
							for (String s: domainNotions){
								System.out.println(s);
							}
							List<Domain> domainList = parseDomains(domainNotions,emptyStack,project.getPackageToken());
							decorateDropdowns(domainList);
							List<Domain> dataDomainList = parseDataDomains(domainList,subNotions,project.getPackageToken(),project.getDbPrefix());
							Project project2 = new Project(project.getStandardName(),project.getPackageToken(),project.getTechnicalstack(),project.getDbUsername(),project.getDbPassword(),project.isEmptypassword(),project.getDbName(),"mysql");
							project2.setDbPrefix(project.getDbPrefix());
							project2.setDbName(project.getDbName());
							project2.setDomains(domainList);
							dbdg.setDomains(domainList);
							project2.addDBDefinitionGenerator(dbdg);
							
							if (locateCallMagic(notions)){
								if (project2.getTechnicalstack().equalsIgnoreCase("jeeeu")){									
									return callMagicClockSimpleJEE(project2,domainList,dbdg,dataDomainList);
								} 
							}
							if (project2.getPackageToken() == null || "".equals(project2.getPackageToken())) {
								ValidateInfo info = new ValidateInfo();
								info.addCompileError("没有设置包名前缀。");
								throw new ValidateException(info);
							}
							if (project2.getTechnicalstack().equalsIgnoreCase("jeeeu")){
								String dbPrefix = project.getDbPrefix();
								for (int m=0;m<domainList.size();m++){
									domainList.get(m).setDbPrefix(dbPrefix);
									domainList.get(m).decorateDomainWithLabels();
								}
								
								List<Prism> prismList = parsePrisms(notions.subList(j, notions.size()),projectStack,domainList,project.getPackageToken());
								project2.setPrisms(prismList);
								if (dataDomainList!=null && dataDomainList.size() > 0) {
									List<List<Domain>> datas = new ArrayList<>();
									datas.add(dataDomainList);
									project2.setDataDomains(datas);
								}
								return project2;
							} 
						}
						}
					}			
				}			
			}
		}
		ValidateInfo info = new ValidateInfo();
		info.addCompileError("将片断缀合成项目错误。");
		throw new ValidateException(info);
	}
	
	public static List<Prism> parsePrisms(List<String> notions, Stack<String> projectStack, List<Domain> domainList, String packageToken) throws Exception{
		List<Prism> list = new ArrayList<Prism>();
		Prism prism = new Prism();
		boolean prismStackjOverflow = false;
		Stack<String> prismStack = new Stack<String>();
		boolean started = false;
		int totalPrismCounts = countPrisms(notions);
		Set<Domain> allDomainSet = new TreeSet<Domain>();
		allDomainSet.addAll(domainList);
		
		for (int i=0; i < notions.size();i++) {
			if ("call".equals(notions.get(i))){
				if ("magic".equals(notions.get(i+1))&& ";".equals(notions.get(i+2))){
					return list;
				}
			}
			
			if  (!"prism".equals(notions.get(i))) {
				notions.remove(i);
				i = i - 1;
			} else {
				break;
			}
		}
		
		for (int i = 0; i < notions.size(); i++){

			if ("prism".equals(notions.get(i))){
				prism = new Prism();
				if (!isKeyword(notions.get(i+1))){
					prism.setStandardName(notions.get(i+1));
				}
				if ("{".equals(notions.get(i+2))){
					prismStack.push("{");
					projectStack.push("{");
				}
				i= i +3;
			}
			
			if ("{".equals(notions.get(i))){
				if (!prismStackjOverflow){
					prismStack.push("{");
				}else{
					prismStackjOverflow = false;
				}
				projectStack.push("{");
			}
			
			if ("}".equals(notions.get(i))){
				if (!prismStack.empty()){
					prismStack.pop();
				} else {
					prismStackjOverflow = true;
				}
				projectStack.pop();
			}
					
			if (prismStack.empty()&&list.size() < totalPrismCounts){
				list.add(prism);
			}else if (prismStack.empty()&&list.size()== totalPrismCounts){
				return list;
			}
			
			if ("prismdomain".equals(notions.get(i))){
				if (":".equals(notions.get(i+1))){
					if (!isKeyword(notions.get(i+2))){
						Domain d = findDomainFromListByStandardName(domainList, notions.get(i+2));
						d.decorateDomainWithLabels();
						if (d!=null) {
							prism.setDomain(d);
						}else {
							throw new ValidateException("没有找到对应的域对象！");
						}
						prism.setPackageToken(packageToken);
						prism.setProjectDomains(allDomainSet);
						prism.generatePrismFromDomain();
						prism.expandPackageToken();
						i = i+2;
						started = true;
					}
				}
			 }	
		 }				
		ValidateInfo info = new ValidateInfo();
		info.addCompileError("解析棱柱错误。");
		throw new ValidateException(info);
	}

	public static  Domain findDomainFromListByStandardName(List<Domain> domainList, String standardName) throws ValidateException{
		for (Domain d:domainList){
			if (d.getStandardName().equals(standardName)) return d;
		}
		ValidateInfo info = new ValidateInfo();
		info.addCompileError("在域对象列表找不到域对象"+standardName+"。");
		throw new ValidateException(info);
	}
	
	public static List<Domain> parseDomains(List<String> notions, Stack<String> projectStack, String packageToken) throws ValidateException{
		List<Domain> list = new ArrayList<Domain>();
		Domain domain = new Domain();
		boolean domainStackjOverflow = false;
		Stack<String> domainStack = new Stack<String>();
		int totalDomainCounts = countDomains(notions);		
		
		for (int i = 0; i < notions.size(); i++){
			if ("call".equals(notions.get(i))){
				if ("magic".equals(notions.get(i+1))&& ";".equals(notions.get(i+2))){
					for (Domain d:list) d.decorateDomainWithLabels();					
					return list;
				}
			}	
			
			if ("domain".equals(notions.get(i))){
				domain = new Domain();
				domain.setPackageToken(packageToken);
				if (isForbidden(notions.get(i+1))){
					ValidateInfo info = new ValidateInfo();
					info.addCompileError("使用了被禁止的单词:"+notions.get(i+1));
					throw new ValidateException(info);
				}
				if (isSqlKeyword(notions.get(i+1))){
					ValidateInfo info = new ValidateInfo();
					info.addCompileError("使用了SQL关键字:"+notions.get(i+1));
					throw new ValidateException(info);
				}
				if (!isKeyword(notions.get(i+1))){
					domain.setStandardName(notions.get(i+1));					
				}
				if ("{".equals(notions.get(i+2))){
					domainStack.push("{");
					projectStack.push("{");
				}
				i= i +3;
				//continue;
			}
			
			if ("{".equals(notions.get(i))){
				if (!domainStackjOverflow){
					domainStack.push("{");
				}else{
					domainStackjOverflow = false;
				}
				projectStack.push("{");
				//continue;
			}
			
			if ("}".equals(notions.get(i))){
				if (!domainStack.empty()){
					domainStack.pop();
				} else {
					domainStackjOverflow = true;
				}
				projectStack.pop();
				//continue;
				if (domainStack.empty() && domainStackjOverflow==false && list.size() < totalDomainCounts){
					list.add(domain);
				}
				if (domainStack.empty()&&list.size()== totalDomainCounts){					
					return list;
				}				
			}			
			
			if ("domainid".equals(notions.get(i))){
				if (notions.get(i+1).equals(":")&& !isKeyword(notions.get(i+2)) && !isKeyword(notions.get(i+3))){
					if (isForbidden(notions.get(i+2))){
						ValidateInfo info = new ValidateInfo();
						info.addCompileError("使用了被禁止的单词:"+notions.get(i+2));
						throw new ValidateException(info);
					}
					if (isSqlKeyword(notions.get(i+2))){
						ValidateInfo info = new ValidateInfo();
						info.addCompileError("使用了SQL关键字:"+notions.get(i+2));
						throw new ValidateException(info);
					}
					Field f = new Field();
					f.setFieldName(notions.get(i+2));
					if (notions.get(i+3).equalsIgnoreCase("long")){
						f.setFieldType("Long");	
					} else if (notions.get(i+3).equals("int")|| notions.get(i+3).equals("Integer")){
						f.setFieldType("Integer");	
					}else {
						ValidateInfo info = new ValidateInfo();
						info.addCompileError("域对象"+domain.getCapFirstDomainName()+"的序号必须是long或者int");
						throw new ValidateException(info);
					}
					domain.setDomainId(f);
					i += 4;
					continue;
				}
		 }	
			
			if ("domainname".equals(notions.get(i))){
				if (notions.get(i+1).equals(":")&& !isKeyword(notions.get(i+2)) && !isKeyword(notions.get(i+3))){
					if (isForbidden(notions.get(i+2))){
						ValidateInfo info = new ValidateInfo();
						info.addCompileError("使用了被禁止的单词:"+notions.get(i+2));
						throw new ValidateException(info);
					}
					if (isSqlKeyword(notions.get(i+2))){
						ValidateInfo info = new ValidateInfo();
						info.addCompileError("使用了SQL关键字:"+notions.get(i+2));
						throw new ValidateException(info);
					}
					Field f = new Field();
					f.setFieldName(notions.get(i+2));
					f.setFieldType("String");					
					domain.setDomainName(f);
					i += 3;
					continue;
				}
		 }
		
			if ("activefield".equals(notions.get(i))){
				if (notions.get(i+1).equals(":")&& !isKeyword(notions.get(i+2)) && !isKeyword(notions.get(i+3))){
					if (isForbidden(notions.get(i+2))){
						ValidateInfo info = new ValidateInfo();
						info.addCompileError("使用了被禁止的单词:"+notions.get(i+2));
						throw new ValidateException(info);
					}
					if (isSqlKeyword(notions.get(i+2))){
						ValidateInfo info = new ValidateInfo();
						info.addCompileError("使用了SQL关键字:"+notions.get(i+2));
						throw new ValidateException(info);
					}
					Field f = new Field();
					f.setFieldName(notions.get(i+2));
					f.setFieldType("boolean");					
					domain.setActive(f);
					i += 3;
					continue;
				}
		 }
			
			if ("plural".equals(notions.get(i))){
				if (notions.get(i+1).equals(":")&& !isKeyword(notions.get(i+2)) && !isKeyword(notions.get(i+3))){
					if (isForbidden(notions.get(i+2))){
						ValidateInfo info = new ValidateInfo();
						info.addCompileError("使用了被禁止的单词:"+notions.get(i+2));
						throw new ValidateException(info);
					}
					if (isSqlKeyword(notions.get(i+2))){
						ValidateInfo info = new ValidateInfo();
						info.addCompileError("使用了SQL关键字:"+notions.get(i+2));
						throw new ValidateException(info);
					}
					String plural = notions.get(i+2);
					domain.setPlural(plural);
					i += 3;
					continue;
				}
			}
			
			if ("domainlabel".equals(notions.get(i))){
				if (notions.get(i+1).equals(":")&& !isKeyword(notions.get(i+2))){
					if (isForbidden(notions.get(i+2))){
						ValidateInfo info = new ValidateInfo();
						info.addCompileError("使用了被禁止的单词："+notions.get(i+2));
						throw new ValidateException(info);
					}
					if (isSqlKeyword(notions.get(i+2))){
						ValidateInfo info = new ValidateInfo();
						info.addCompileError("使用了SQL关键字："+notions.get(i+2));
						throw new ValidateException(info);
					}
					String label = notions.get(i+2);
					domain.setLabel(label);
					i += 3;
					continue;
				}
			}
			
			if ("field".equals(notions.get(i))){
				if (notions.get(i+1).equals(":")&& !isKeyword(notions.get(i+2)) && !isKeyword(notions.get(i+3)) && ";".equals(notions.get(i+4))){
					if (isForbidden(notions.get(i+2))){
						ValidateInfo info = new ValidateInfo();
						info.addCompileError("使用了被禁止的单词:"+notions.get(i+2));
						throw new ValidateException(info);
					}
					if (isSqlKeyword(notions.get(i+2))){
						ValidateInfo info = new ValidateInfo();
						info.addCompileError("使用了SQL关键字:"+notions.get(i+2));
						throw new ValidateException(info);
					}
					if (!StringUtil.isLowerCaseLetter(notions.get(i+2))){
						throw new ValidateException("字段"+notions.get(i+2)+"未使用小写英文字母开头！");
					}
					if (!StringUtil.isLowerCaseLetterPosition(notions.get(i+2),1)){
						throw new ValidateException("字段"+notions.get(i+2)+"第二个字母未使用小写英文字母！");
					}
					Type fieldType= new Type(notions.get(i+3)).getClassType();
					Field f = new Field(notions.get(i+2),fieldType);
					domain.addField(f);	
					i += 3;
				}else if (!isKeyword(notions.get(i+4)) && !";".equals(notions.get(i+4))&&!"}".equals(notions.get(i+4))){
					if (isForbidden(notions.get(i+2))){
						ValidateInfo info = new ValidateInfo();
						info.addCompileError("使用了被禁止的单词："+notions.get(i+2));
						throw new ValidateException(info);
					}
					if (isSqlKeyword(notions.get(i+2))){
						ValidateInfo info = new ValidateInfo();
						info.addCompileError("使用了SQL关键字："+notions.get(i+2));
						throw new ValidateException(info);
					}
					if (!StringUtil.isLowerCaseLetter(notions.get(i+2))){
						throw new ValidateException("字段"+notions.get(i+2)+"未使用小写英文字母开头！");
					}
					if (!StringUtil.isLowerCaseLetterPosition(notions.get(i+2),1)){
						throw new ValidateException("字段"+notions.get(i+2)+"第二个字母未使用小写英文字母！");
					}
					Field f = new Field(notions.get(i+2),notions.get(i+3),notions.get(i+4));
					domain.addField(f);	
					i += 4;
				}						
				continue;
			}
			
			
			if ("dropdown".equals(notions.get(i))){
				if (notions.get(i+1).equals(":")&& !isKeyword(notions.get(i+2)) && !isKeyword(notions.get(i+3)) && ";".equals(notions.get(i+4))){
					if (isForbidden(notions.get(i+2))){
						ValidateInfo info = new ValidateInfo();
						info.addCompileError("使用了被禁止的单词："+notions.get(i+2));
						throw new ValidateException(info);
					}
					if (isSqlKeyword(notions.get(i+2))){
						ValidateInfo info = new ValidateInfo();
						info.addCompileError("使用了SQL关键字："+notions.get(i+2));
						throw new ValidateException(info);
					}
					if (isForbidden(notions.get(i+3))){
						ValidateInfo info = new ValidateInfo();
						info.addCompileError("使用了被禁止的单词："+notions.get(i+3));
						throw new ValidateException(info);
					}
					if (isSqlKeyword(notions.get(i+3))){
						ValidateInfo info = new ValidateInfo();
						info.addCompileError("使用了SQL关键字："+notions.get(i+3));
						throw new ValidateException(info);
					}
					if (!StringUtil.isLowerCaseLetter(notions.get(i+3))){
						throw new ValidateException("字段"+notions.get(i+3)+"未使用小写英文字母开头！");
					}
					if (!StringUtil.isLowerCaseLetterPosition(notions.get(i+3),1)){
						throw new ValidateException("字段"+notions.get(i+3)+"第二个字母未使用小写英文字母！");
					}
					Dropdown dp = new Dropdown(notions.get(i+2));	
					dp.setAliasName(notions.get(i+3));
					dp.setFieldName(dp.getAliasName());
					domain.addField(dp);	
					
					i += 3;
				}						
				continue;
			}
			
			if ("labelfield".equals(notions.get(i))){
				if (notions.get(i+1).equals(":")&& !isKeyword(notions.get(i+2)) && !isKeyword(notions.get(i+3)) && ";".equals(notions.get(i+4))){
					if (isForbidden(notions.get(i+2))){
						ValidateInfo info = new ValidateInfo();
						info.addCompileError("使用了被禁止的单词："+notions.get(i+2));
						throw new ValidateException(info);
					}
					if (isSqlKeyword(notions.get(i+2))){
						ValidateInfo info = new ValidateInfo();
						info.addCompileError("使用了SQL关键字："+notions.get(i+2));
						throw new ValidateException(info);
					}
					domain.putFieldLabel(notions.get(i+2),notions.get(i+3));
					i += 3;
				}						
				continue;
			}
			
			if ("manytomanyslave".equals(notions.get(i))){
				if (notions.get(i+1).equals(":")&& !isKeyword(notions.get(i+2)) && ";".equals(notions.get(i+3))){
					if (isForbidden(notions.get(i+2))){
						ValidateInfo info = new ValidateInfo();
						info.addCompileError("使用了被禁止的单词："+notions.get(i+2));
						throw new ValidateException(info);
					}
					if (isSqlKeyword(notions.get(i+2))){
						ValidateInfo info = new ValidateInfo();
						info.addCompileError("使用了SQL关键字："+notions.get(i+2));
						throw new ValidateException(info);
					}
					
					domain.addManyToManySlaveName(notions.get(i+2));
					i += 2;
				}						
				continue;
			}		

			if (";".equals(notions.get(i))) continue;
		}				
		ValidateInfo info = new ValidateInfo();
		info.addCompileError("解析域对象列表错误！");
		throw new ValidateException(info);
	}
	
	public static boolean isKeyword(String notion){
		if ("project".equals(notion)||"prism".equals(notion)||"domain".equals(notion)||"field".equals(notion)
				||"packagetoken".equals(notion)||"plural".equals(notion)||"activefield".equals(notion)
				||"domainname".equals(notion)||"domainid".equals(notion)||"prismdomain".equals(notion)
				||"dbname".equals(notion)|| "emptypassword".equals(notion) ||"call".equals(notion) || "magic".equals(notion)) return true;
		else return false;
	}
	
	public static int countDomains(List<String> notions){
		int count = 0;
		for (String s:notions){
			if (s.equals("domain")) count ++;
		}
		return count;
	}
	
	public static int countPrisms(List<String> notions){
		int count = 0;
		for (String s:notions){
			if (s.equals("prism")) count ++;
		}
		return count;
	}
	
	public static boolean containsRoundAndSquareBrackets(String source){
		return source.contains("[") || source.contains("]")|| source.contains("(")|| source.contains(")");
	}
	
	public static ValidateInfo validateDomainsAndPrisms(List<Domain> domains,List<Prism> prisms){
		ValidateInfo validateInfo1 = validateDomains(domains);
		ValidateInfo validateInfo2 = validatePrisms(prisms);
		List<ValidateInfo> vList = new ArrayList<ValidateInfo>();
		vList.add(validateInfo1);
		vList.add(validateInfo2);
		ValidateInfo validateInfo = ValidateInfo.mergeValidateInfo(vList);
		return validateInfo;
	}
	
	public static ValidateInfo validateDomains(List<Domain> domains){
		ValidateInfo validateInfo = new ValidateInfo();
		List<Domain> targets = new ArrayList<Domain>();
		for (int i=0;i<domains.size();i++){
			for (int j=0;j<targets.size();j++){
				if (domains.get(i).getStandardName().equals(targets.get(j).getStandardName())) {
					validateInfo.addCompileWarning("域对象"+ domains.get(i).getStandardName()+"重复。");
				}else{
					targets.add(domains.get(i));
				}
			}
		}
		return validateInfo;
	}
	
	public static ValidateInfo validatePrisms(List<Prism> prisms){
		ValidateInfo validateInfo = new ValidateInfo();
		List<Prism> targets = new ArrayList<Prism>();
		for (int i=0;i<prisms.size();i++){
			for (int j=0;j<targets.size();j++){
				if (prisms.get(i).getStandardName().equals(targets.get(j).getStandardName())) {
					validateInfo.addCompileWarning("棱柱"+ prisms.get(i).getStandardName()+"重复。");
				}else{
					targets.add(prisms.get(i));
				}
			}
		}
		if (prisms != null){
			for (int i=0;i<prisms.size();i++){
				if (!prisms.get(i).getStandardName().equals(prisms.get(i).getDomain().getStandardName()+"Prism")){
					validateInfo.addCompileWarning("棱柱"+prisms.get(i).getStandardName() + "的域对象"+prisms.get(i).getDomain().getStandardName()+"没有正确设置。");
				}
			}
		}
		return validateInfo;
	}
	
	public static ValidateInfo basicValidateProject(Project project){
		List<Prism> prisms = project.getPrisms();
		List<Domain> domains = project.getDomains();
		return validateDomainsAndPrisms(domains, prisms);
	}
		
	public static Project callMagicClockSimpleJEE(Project project,List<Domain> domainList,DBDefinitionGenerator dbdg,List<Domain> dataDomainList) throws Exception{
		decorateDropdowns(domainList);
		List<Prism> prismList = generateClockSimpleJeePrismsByDomains(domainList,dbdg);
		project.mergeDomains(domainList);
		project.setPrisms(prismList);
		if (dataDomainList != null && dataDomainList.size()>0) {
			List<List<Domain>> datas = new ArrayList<>();
			datas.add(dataDomainList);
			project.setDataDomains(datas);
		}
		return project;
	}
	
	public static boolean locateCallMagic(List<String> notions){
		if (notions.contains("call")&&notions.contains("magic")) return true;
		else return false;
	}
	
	public static List<Prism> generateClockSimpleJeePrismsByDomains(List<Domain> domainList, DBDefinitionGenerator dbdg) throws Exception{
		List<Prism> prisms = new ArrayList<>();
		Set<Domain> projectDomainSet = new TreeSet<Domain>();
		projectDomainSet.addAll(domainList);
		for (Domain d:domainList){
			d.decorateDomainWithLabels();
			Prism p = new Prism();			
			p.setStandardName(d.getCapFirstDomainName()+"Prism");
			p.setPackageToken(d.getPackageToken());
			p.setDomain(d);			
			p.setDbDefinitionGenerator(dbdg);
			p.setProjectDomains(projectDomainSet);
			p.generatePrismFromDomain();
			prisms.add(p);
		}
		return prisms;
	}
	
	
	public static List<String> fixDomainsNotions(List<String> notions, Stack<String> myStack){
		return removeNotions(notions,myStack.size());
	}
	
	public static List<String> removeNotions(List<String> notions, int count){
		if (notions == null || notions.size() < 1) return new ArrayList<String>();
		int endFlag = notions.size() -1;
		for (int i=notions.size()-1;i>=0;i--){
			if (!notions.get(i).equals("}") && count >= 0){
				endFlag --;
			}else if (notions.get(i).equals("}") && count > 0){
				count --;
				endFlag--;
			}else if (notions.get(i).equals("}") && count ==0){
				return notions.subList(0,endFlag+1);
			}
		}
		return notions;
	}
	
	public static void decorateDropdowns(List<Domain> domainList) throws ValidateException{
		for (Domain d:domainList){
			for (Field f: d.getFieldsWithoutId()){
				if (f instanceof Dropdown){
					Dropdown dp = (Dropdown)f;
					System.out.println("JerryDebugger:dropdown:"+ dp.getTargetName());
					Domain t = DomainUtil.findDomainInList(domainList, dp.getTargetName());
					dp.decorate(t);
				}
			}
		}
	}
	
	public static List<Domain> parseDataDomains(List<Domain> targetDomains,List<String> notions, String packageToken, String dbPrefix) throws ValidateException{
		List<Domain> list = new ArrayList<Domain>();
		Domain domain = new Domain();
		boolean domainStackjOverflow = false;
		Stack<String> domainStack = new Stack<String>();
		boolean datadomainStart = false;
		List<String> notions2 = clipDataDomainNotions(notions);
		
		if (notions2 == null || notions2.size() == 0) {
			return list;
		}
		System.out.print("JerryDebug:notions2:");
		for (String s:notions2) {
			System.out.print(s);
		}
		System.out.println();
		
		for (int i = 0; i < notions2.size(); i++){
			if ("datadomain".equals(notions2.get(i))){
				String domainName = notions2.get(i+1);
				domain = (Domain)findDomainFromListByStandardName(targetDomains, domainName).deepClone();
				domain.setDbPrefix(dbPrefix);
				datadomainStart = true;
				if ("{".equals(notions2.get(i+2))){
					domainStack.push("{");
				}
				i= i +3;
				//continue;
			}
			
			if ("{".equals(notions2.get(i))){
				if (!domainStackjOverflow){
					domainStack.push("{");
				}else{
					domainStackjOverflow = false;
				}
				//continue;
			}
			
			if ("}".equals(notions2.get(i))){
				if (!domainStack.empty()){
					domainStack.pop();
				} else {
					domainStackjOverflow = true;
				}
				//continue;
				if (datadomainStart){
					list.add(domain);
					datadomainStart=false;
				}
				if (i>=notions2.size()-1){					
					return list;
				}				
			}			
			
			if (datadomainStart&&"domainid".equals(notions2.get(i))){
				if (notions2.get(i+1).equals(":")){		
					domain.setFieldValue(notions2.get(i+2),notions2.get(i+3));
					i += 3;
					continue;
				}
		 }	
			
			if (datadomainStart&&"domainname".equals(notions2.get(i))){
				if (notions2.get(i+1).equals(":")){
					domain.setFieldValue(notions2.get(i+2),notions2.get(i+3));
					i += 3;
					continue;
				}
		 }
		
			if (datadomainStart&&"activefield".equals(notions2.get(i))){
				if (notions2.get(i+1).equals(":")&& !isKeyword(notions2.get(i+2)) ){
					domain.setFieldValue(notions2.get(i+2),notions2.get(i+3));
					i += 3;
					continue;
				}
		 }
			
			if (datadomainStart&&"field".equals(notions2.get(i))){
				if (notions2.get(i+1).equals(":")&& !isKeyword(notions2.get(i+2))){
					domain.setFieldValue(notions2.get(i+2),notions2.get(i+3));
					i += 3;
					continue;
				}				
			}
			
			if (datadomainStart&&"dropdown".equals(notions2.get(i))){
				if (notions2.get(i+1).equals(":")&& !isKeyword(notions2.get(i+2)) && !isKeyword(notions2.get(i+3)) && ";".equals(notions2.get(i+4))){
					Field f = domain.getField(notions2.get(i+2));
					Dropdown dp = (Dropdown) f;
					String fieldValue = notions2.get(i+3);
					if (!StringUtil.isBlank(fieldValue))
						domain.setFieldValue(dp.getAliasName(), fieldValue);
					else
						domain.setFieldValue(dp.getAliasName(), fieldValue);					
					i += 3;
				}						
				continue;
			}
		
			if ("manytomanyslave".equals(notions2.get(i))){
				//domain.addManyToManySlaveName(notions2.get(i+2));
				i += 2;		
				continue;
			}			

			if (";".equals(notions2.get(i))) continue;
		}				
		ValidateInfo info = new ValidateInfo();
		info.addCompileError("解析域对象错误。");
		throw new ValidateException(info);
	}
	
	public static List<String> clipDataDomainNotions(List<String> notions){
		List<String> results = new ArrayList<String>();
		boolean started = false;
		for (int i = 0; i < notions.size(); i++){
			if ("datadomain".equals(notions.get(i))) {
				started = true;
			}else if ("prism".equals(notions.get(i))||"call".equals(notions.get(i))) {
				started = false;
			}
			if(started) results.add(notions.get(i));
		}
		return results;
	}
}