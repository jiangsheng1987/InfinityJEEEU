package org.javaforever.infinity.domain;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.javaforever.infinity.core.Verb;
import org.javaforever.infinity.easyui.EasyUIPageTemplate;
import org.javaforever.infinity.exception.ValidateException;
import org.javaforever.infinity.generator.DBDefinitionFactory;
import org.javaforever.infinity.generator.DBDefinitionGenerator;
import org.javaforever.infinity.generator.NamedUtilMethodGenerator;
import org.javaforever.infinity.limitedverb.CountActiveRecords;
import org.javaforever.infinity.limitedverb.CountAllRecords;
import org.javaforever.infinity.limitedverb.CountPage;
import org.javaforever.infinity.limitedverb.CountSearchByFieldsRecords;
import org.javaforever.infinity.limitedverb.DaoOnlyVerb;
import org.javaforever.infinity.limitedverb.NoControllerVerb;
import org.javaforever.infinity.utils.StringUtil;
import org.javaforever.infinity.verb.Add;
import org.javaforever.infinity.verb.Delete;
import org.javaforever.infinity.verb.DeleteAll;
import org.javaforever.infinity.verb.FindById;
import org.javaforever.infinity.verb.FindByName;
import org.javaforever.infinity.verb.ListActive;
import org.javaforever.infinity.verb.ListAll;
import org.javaforever.infinity.verb.ListAllByPage;
import org.javaforever.infinity.verb.SearchByFieldsByPage;
import org.javaforever.infinity.verb.SearchByName;
import org.javaforever.infinity.verb.SoftDelete;
import org.javaforever.infinity.verb.SoftDeleteAll;
import org.javaforever.infinity.verb.Toggle;
import org.javaforever.infinity.verb.ToggleOne;
import org.javaforever.infinity.verb.Update;

public class Prism implements Comparable{
	protected long prismId;
	protected String standardName;
	protected long namingId;
	protected Naming naming;
	protected long domainClassId;
	protected Domain domain;
	protected long daoimplClassId;
	protected long serviceimplClassId;
	protected long daoId;
	protected Dao dao;
	protected DaoImpl daoimpl;
	protected long serviceId;
	protected Service service;
	protected ServiceImpl serviceimpl;
	protected String prismComment;
	protected List<Class> classes = new ArrayList<Class>(); 
	protected List<Util> utils = new ArrayList<Util>();
	protected List<Controller> controllers = new ArrayList<Controller>();
	protected List<Facade> facades = new ArrayList<Facade>();
	protected String folderPath = "D:/JerryWork/Infinity/testFiles/";
	protected DBDefinitionGenerator dbDefinitionGenerator; 
	protected List<EasyUIPageTemplate> euTemplates = new ArrayList<EasyUIPageTemplate>();
	protected String packageToken;
	protected TestSuite prismTestSuite;
	protected TestCase daoImplTestCase;
	protected TestCase serviceImplTestCase;
	protected List<Verb> verbs = new ArrayList<Verb>();
	protected List<NoControllerVerb> noControllerVerbs = new ArrayList<NoControllerVerb>();
	protected List<DaoOnlyVerb> daoOnlyVerbs = new ArrayList<DaoOnlyVerb>();
	protected List<ManyToMany> manyToManies = new ArrayList<ManyToMany>();
	protected Set<Domain> projectDomains = new TreeSet<Domain>();
	protected List<TwoDomainFacade> twoDomainFacades = new ArrayList<TwoDomainFacade>();
	
	public List<TwoDomainFacade> getTwoDomainFacades() {
		return twoDomainFacades;
	}
	public void setTwoDomainFacades(List<TwoDomainFacade> twoDomainFacades) {
		this.twoDomainFacades = twoDomainFacades;
	}
	public void addTwoDomainFacade(TwoDomainFacade twoDomainFacade) {
		this.twoDomainFacades.add(twoDomainFacade);
	}
	
	public Set<Domain> getProjectDomains() {
		return projectDomains;
	}
	public void setProjectDomains(Set<Domain> projectDomains) {
		this.projectDomains = projectDomains;
	}
	public List<Facade> getFacades() {
		return facades;
	}
	public void setFacades(List<Facade> facades) {
		this.facades = facades;
	}
	
	public List<NoControllerVerb> getNoControllerVerbs() {
		return noControllerVerbs;
	}
	public void setNoControllerVerbs(List<NoControllerVerb> noControllerVerbs) {
		this.noControllerVerbs = noControllerVerbs;
	}
	public List<DaoOnlyVerb> getDaoOnlyVerbs() {
		return daoOnlyVerbs;
	}
	public void setDaoOnlyVerbs(List<DaoOnlyVerb> daoOnlyVerbs) {
		this.daoOnlyVerbs = daoOnlyVerbs;
	}
	
	public List<Util> getUtils() {
		return utils;
	}
	public void setUtils(List<Util> utils) {
		this.utils = utils;
	}
	public void addUtil(Util util){
		this.utils.add(util);
	}
	public long getPrismId() {
		return prismId;
	}
	public List<Class> getClasses() {
		return classes;
	}
	public void setClasses(List<Class> classes) {
		this.classes = classes;
	}
	public void addClass(Class clazz){
		this.classes.add(clazz);
	}
	public void setPrismId(long prismId) {
		this.prismId = prismId;
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public long getNamingId() {
		return namingId;
	}
	public void setNamingId(long namingId) {
		this.namingId = namingId;
	}
	public Naming getNaming() {
		return naming;
	}
	public void setNaming(Naming naming) {
		this.naming = naming;
	}
	public long getDomainClassId() {
		return domainClassId;
	}
	public void setDomainClassId(long domainClassId) {
		this.domainClassId = domainClassId;
	}
	public Domain getDomain() {
		return domain;
	}
	public void setDomain(Domain domain) {
		this.domain = domain;
	}
	public long getDaoimplClassId() {
		return daoimplClassId;
	}
	public void setDaoimplClassId(long daoimplClassId) {
		this.daoimplClassId = daoimplClassId;
	}
	public DaoImpl getDaoimpl() {
		return this.daoimpl;
	}
	public void setDaoimpl(DaoImpl daoimpl) {
		this.daoimpl = daoimpl;
	}
	public long getServiceImplClassId() {
		return serviceimplClassId;
	}
	public void setServiceImplClassId(long serviceimplClassId) {
		this.serviceimplClassId = serviceimplClassId;
	}
	public ServiceImpl getServiceImpl() {
		return serviceimpl;
	}
	public void setServiceImpl(ServiceImpl serviceimpl) {
		this.serviceimpl = serviceimpl;
	}
	public long getDaoId() {
		return daoId;
	}
	public void setDaoId(long daoId) {
		this.daoId = daoId;
	}
	public Dao getDao() {
		return dao;
	}
	public void setDao(Dao dao) {
		this.dao = dao;
	}
	public long getServiceId() {
		return serviceId;
	}
	public void setServiceId(long serviceId) {
		this.serviceId = serviceId;
	}
	public Service getService() {
		return service;
	}
	public void setService(Service service) {
		this.service = service;
	}
	public String getPrismComment() {
		return prismComment;
	}
	public void setPrismComment(String prismComment) {
		this.prismComment = prismComment;
	}
	
	public void generatePrismFiles() throws ValidateException{
		ValidateInfo info = this.validate();
		if (info.success() == false) {
			ValidateException e = new ValidateException(info);
			throw e;
		}
		try {
			String srcfolderPath = folderPath;
			if (this.packageToken != null && !"".equals(this.packageToken))
				srcfolderPath = folderPath + "src/" + packagetokenToFolder(this.packageToken);
			writeToFile(srcfolderPath + "domain/" + StringUtil.capFirst(this.getDomain().getStandardName())+ ".java", this.getDomain().generateClassString());
	        
	        if (this.getDao()!=null){
	        	writeToFile(srcfolderPath + "dao/" + StringUtil.capFirst(this.getDomain().getStandardName())+ "Dao.java", this.getDao().generateDaoString());
	        }
	        
	        if (this.getDaoImpl() != null) {
	        	writeToFile(srcfolderPath + "daoimpl/" + StringUtil.capFirst(this.getDomain().getStandardName())+"DaoImpl.java",this.getDaoImpl().generateDaoImplString());
	        }
	        
	        if (this.getService() != null) {
	        	writeToFile(srcfolderPath + "service/" + StringUtil.capFirst(this.getDomain().getStandardName())+"Service.java", this.getService().generateServiceString());
	        }
	        
	        if (this.getServiceImpl() != null) {
	        	writeToFile(srcfolderPath + "serviceimpl/" +StringUtil.capFirst(this.getDomain().getStandardName())+"ServiceImpl.java", this.getServiceImpl().generateServiceImplString());
	        }
	        
	        for (Facade facade : this.facades){
	        	writeToFile(srcfolderPath + "facade/" + StringUtil.capFirst(facade.getStandardName())+".java", facade.generateFacadeString());
	        }
	        
	        for (TwoDomainFacade twfacade : this.twoDomainFacades){	        	
	        	writeToFile(srcfolderPath + "facade/" + StringUtil.capFirst(twfacade.getStandardName()) + ".java", twfacade.generateFacadeString());
	        }
	        
	        for (EasyUIPageTemplate eutp : this.euTemplates){
	        	writeToFile(folderPath + "WebContent/pages/" + this.domain.getPlural().toLowerCase()+".html", eutp.generateJspString());
	        }
  
			for (ManyToMany mtm : this.manyToManies) {
				writeToFile(folderPath + "WebContent/pages/" + mtm.getEuTemplate().getStandardName().toLowerCase() + ".html",
						mtm.getEuTemplate().generateContentString());
			}
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public String getFolderPath() {
		return folderPath;
	}
	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}
	public DaoImpl getDaoImpl() {
		return daoimpl;
	}
	public void setDaoImpl(DaoImpl daoImpl) {
		this.daoimpl = daoImpl;
	}
	
	public void addController(Controller c){
		this.controllers.add(c);
	}
	public long getServiceimplClassId() {
		return serviceimplClassId;
	}
	public void setServiceimplClassId(long serviceimplClassId) {
		this.serviceimplClassId = serviceimplClassId;
	}
	public ServiceImpl getServiceimpl() {
		return serviceimpl;
	}
	public void setServiceimpl(ServiceImpl serviceimpl) {
		this.serviceimpl = serviceimpl;
	}
	public List<Controller> getControllers() {
		return controllers;
	}
	public void setControllers(List<Controller> controllers) {
		this.controllers = controllers;
	}
	public DBDefinitionGenerator getDbDefinitionGenerator() {
		return dbDefinitionGenerator;
	}
	public void setDbDefinitionGenerator(DBDefinitionGenerator dbDefinitionGenerator) {
		this.dbDefinitionGenerator = dbDefinitionGenerator;
	}

	public String getPackageToken() {
		return packageToken;
	}
	public void setPackageToken(String packagetoken) {
		if (packagetoken != null){
			this.packageToken = packagetoken;
		}
	}
	
	public void generatePrismFromDomain() throws Exception{
		if (this.domain != null){
			if (this.getPackageToken() != null){
				this.domain.setPackageToken(packageToken);
			}
				
			this.dao = new Dao();
			this.dao.setDomain(this.domain);
			this.dao.setPackageToken(this.packageToken);
			this.daoimpl = new DaoImpl();
			this.daoimpl.setPackageToken(this.packageToken);
			this.daoimpl.setDomain(this.domain);
			this.daoimpl.setDao(this.dao);
			
			this.service = new Service();
			this.service.setDomain(this.domain);
			this.service.setPackageToken(this.packageToken);
			this.serviceimpl = new ServiceImpl(this.domain);
			this.serviceimpl.setDomain(this.domain);
			this.serviceimpl.setPackageToken(this.packageToken);
			this.serviceimpl.setService(this.service);
			
			Verb listAll = new ListAll(this.domain);
			Verb update = new Update(this.domain);
			Verb delete = new Delete(this.domain);
			Verb add = new Add(this.domain);
			Verb softdelete = new SoftDelete(this.domain);
			Verb findbyid = new FindById(this.domain);
			Verb findbyname = new FindByName(this.domain);
			Verb searchbyname = new SearchByName(this.domain);
			Verb listactive = new ListActive(this.domain);
			Verb listAllByPage = new ListAllByPage(this.domain);
			Verb deleteAll = new DeleteAll(this.domain);
			Verb softDeleteAll = new SoftDeleteAll(this.domain);
			Verb toggle = new Toggle(this.domain);
			Verb toggleOne = new ToggleOne(this.domain);
			Verb searchByFieldsByPage = new SearchByFieldsByPage(this.domain);
			
			CountPage countPage = new CountPage(this.domain);
			CountActiveRecords countActiveRecords = new CountActiveRecords(this.domain);
			CountAllRecords countAllRecords = new CountAllRecords(this.domain);
			CountSearchByFieldsRecords countSearch = new CountSearchByFieldsRecords(this.domain);
			
			this.addVerb(listAll);
			this.addVerb(update);
			this.addVerb(delete);
			this.addVerb(add);
			this.addVerb(softdelete);
			this.addVerb(findbyid);
			this.addVerb(findbyname);
			this.addVerb(searchbyname);
			this.addVerb(listactive);
			this.addVerb(listAllByPage);
			this.addVerb(deleteAll);
			this.addVerb(softDeleteAll);
			this.addVerb(toggle);
			this.addVerb(toggleOne);
			this.addVerb(searchByFieldsByPage);
			
			this.noControllerVerbs.add(countPage);
			this.noControllerVerbs.add(countActiveRecords);
			this.noControllerVerbs.add(countAllRecords);
			this.noControllerVerbs.add(countSearch);
			
			for (Verb v: this.verbs){
				v.setDomain(domain);
				Facade facade = new Facade(v,domain);
				facade.setPackageToken(domain.getPackageToken());
				this.facades.add(facade);
				service.addMethod(v.generateServiceMethodDefinition());
				serviceimpl.addMethod(v.generateServiceImplMethod());
				dao.addMethod(v.generateDaoMethodDefinition());
				daoimpl.addMethod(v.generateDaoImplMethod());
			}
			
			for (NoControllerVerb nVerb: this.noControllerVerbs){
				nVerb.setDomain(domain);
				service.addMethod(nVerb.generateServiceMethodDefinition());
				serviceimpl.addMethod(nVerb.generateServiceImplMethod());
				dao.addMethod(nVerb.generateDaoMethodDefinition());
				daoimpl.addMethod(nVerb.generateDaoImplMethod());				
			}
			
			for (DaoOnlyVerb oVerb: this.daoOnlyVerbs){
				oVerb.setDomain(domain);
				dao.addMethod(oVerb.generateDaoMethodDefinition());
				daoimpl.addMethod(oVerb.generateDaoImplMethod());				
			}
			
			DBDefinitionGenerator dbg = DBDefinitionFactory.getInstance("mysql");
			dbg.addDomain(this.domain);
			dbg.setDbName(this.domain.standardName);
			this.setDbDefinitionGenerator(dbg);	
								
			EasyUIPageTemplate easyui = new EasyUIPageTemplate();
			easyui.setDomain(this.domain);
			easyui.setStandardName(this.domain.getStandardName().toLowerCase());
			this.addEuTemplate(easyui);
			
			if (this.domain.manyToManySlaveNames != null && this.domain.manyToManySlaveNames.size() > 0) {
				for (String slaveName : this.domain.manyToManySlaveNames) {
					String masterName = this.domain.getStandardName();
					if (setContainsDomain(this.projectDomains, masterName)
							&& setContainsDomain(this.projectDomains, slaveName)) {
						this.manyToManies.add(new ManyToMany(lookupDoaminInSet(this.projectDomains, masterName),
								lookupDoaminInSet(this.projectDomains, slaveName)));
					} else {
						ValidateInfo validateInfo = new ValidateInfo();
						validateInfo.addCompileError("棱柱" + this.getStandardName() + "多对多设置有误。");
						ValidateException em = new ValidateException(validateInfo);
						throw em;
					}
				}
			}
			for (ManyToMany mtm : this.manyToManies) {
				this.service.addMethod(mtm.getAssign().generateServiceMethodDefinition());
				this.serviceimpl.addMethod(mtm.getAssign().generateServiceImplMethod());
				this.dao.addMethod(mtm.getAssign().generateDaoMethodDefinition());
				this.daoimpl.addMethod(mtm.getAssign().generateDaoImplMethod());
				//this.mybatisDaoXmlDecorator.addDaoXmlMethod(mtm.getAssign().generateDaoImplMethod());
				//this.facade.addMethod(mtm.getAssign().generateFacadeMethod());

				this.service.addMethod(mtm.getRevoke().generateServiceMethodDefinition());
				this.serviceimpl.addMethod(mtm.getRevoke().generateServiceImplMethod());
				this.dao.addMethod(mtm.getRevoke().generateDaoMethodDefinition());
				this.daoimpl.addMethod(mtm.getRevoke().generateDaoImplMethod());
				//this.mybatisDaoXmlDecorator.addDaoXmlMethod(mtm.getRevoke().generateDaoImplMethod());
				//this.facade.addMethod(mtm.getRevoke().generateFacadeMethod());

				this.service.addMethod(mtm.getListMyActive().generateServiceMethodDefinition());
				this.serviceimpl.addMethod(mtm.getListMyActive().generateServiceImplMethod());
				this.dao.addMethod(mtm.getListMyActive().generateDaoMethodDefinition());
				this.daoimpl.addMethod(mtm.getListMyActive().generateDaoImplMethod());
//				this.mybatisDaoXmlDecorator.addResultMap(mtm.getSlave());
//				this.mybatisDaoXmlDecorator.addDaoXmlMethod(mtm.getListMyActive().generateDaoImplMethod());
//				this.facade.addMethod(mtm.getListMyActive().generateFacadeMethod());

				this.service.addMethod(mtm.getListMyAvailableActive().generateServiceMethodDefinition());
				this.serviceimpl.addMethod(mtm.getListMyAvailableActive().generateServiceImplMethod());
				this.dao.addMethod(mtm.getListMyAvailableActive().generateDaoMethodDefinition());
				this.daoimpl.addMethod(mtm.getListMyAvailableActive().generateDaoImplMethod());
//				this.mybatisDaoXmlDecorator.addDaoXmlMethod(mtm.getListMyAvailableActive().generateDaoImplMethod());
//				this.facade.addMethod(mtm.getListMyAvailableActive().generateFacadeMethod());
				mtm.slave.decorateCompareTo();
				
				Service slaveService = new Service();
				slaveService.setDomain(mtm.slave);
				slaveService.setStandardName(mtm.slave.getCapFirstDomainName() + "Service");
				
				Method slaveServiceSetter = NamedUtilMethodGenerator.generateSetter(mtm.slave.getLowerFirstDomainName()+"Service",
						new Type(mtm.slave.getCapFirstDomainName() + "Service",mtm.getSlave().getPackageToken()+".service."+mtm.slave.getCapFirstDomainName() + "Service"));
				this.serviceimpl.addMethod(slaveServiceSetter);
				this.serviceimpl.addClassImports(mtm.slave.getPackageToken()+".serviceimpl."+mtm.slave.getCapFirstDomainName() + "ServiceImpl");
				this.serviceimpl.addOtherService(slaveService);
			}
		}
	}

	public static String packagetokenToFolder(String packageToken){
		String folder = packageToken.replace('.','/');
		folder += "/";
		return folder;
	}
	
	public static String folderToPackageToken(String folder){
		String packagetoken = folder.replace('/', '.');
		if (packagetoken.charAt(packagetoken.length()-1) == '.')
			packagetoken = packagetoken.substring(0, packagetoken.length() -1);
		return packagetoken;
	}

	public TestSuite getPrismTestSuite() {
		return prismTestSuite;
	}
	public void setPrismTestSuite(TestSuite prismTestSuite) {
		this.prismTestSuite = prismTestSuite;
	}
	public TestCase getDaoImplTestCase() {
		return daoImplTestCase;
	}
	public void setDaoImplTestCase(TestCase daoImplTestCase) {
		this.daoImplTestCase = daoImplTestCase;
	}
	public TestCase getServiceImplTestCase() {
		return serviceImplTestCase;
	}
	public void setServiceImplTestCase(TestCase serviceImplTestCase) {
		this.serviceImplTestCase = serviceImplTestCase;
	}
	
	public ValidateInfo validate(){
		List <ValidateInfo> vl = new ArrayList<ValidateInfo>();
		ValidateInfo info = this.getDomain().validate();
		vl.add(info);
		for (Controller c: this.controllers){
			ValidateInfo info2 = c.validate();
			vl.add(info2);
		}
		return ValidateInfo.mergeValidateInfo(vl);
	}
	
	public void expandPackageToken(){
		if (this.packageToken!=null && !"".equals(this.packageToken)){
			if (this.domain != null) this.domain.setPackageToken(this.packageToken);
			if (this.dao != null) this.dao.setPackageToken(this.packageToken);
			if (this.daoimpl != null) this.daoimpl.setPackageToken(this.packageToken);
			if (this.service != null) this.service.setPackageToken(this.packageToken);
			if (this.serviceimpl != null) this.serviceimpl.setPackageToken(this.packageToken);

			for (Class c : this.classes) {
				c.setPackageToken(this.packageToken);
			}
			for (Controller ctl: this.controllers){
				ctl.setPackageToken(this.packageToken);
			}
			
			if (this.prismTestSuite != null) this.prismTestSuite.setPackageToken(this.packageToken);
			if (this.daoImplTestCase != null) this.daoImplTestCase.setPackageToken(this.packageToken);
			if (this.serviceImplTestCase != null) this.serviceImplTestCase.setPackageToken(this.packageToken);
		}
	}
	@Override
	public int compareTo(Object o) {
		String myName = this.getStandardName();
		String otherName = ((Prism)o).getStandardName();
		return myName.compareTo(otherName);
	}

	
	@Override
	public boolean equals(Object o){
		return (this.compareTo((Prism)o)==0);
	}
	public List<Verb> getVerbs() {
		return verbs;
	}
	public void setVerbs(List<Verb> verbs) {
		this.verbs = verbs;
	}
	
	public void addVerb(Verb verb){
		this.verbs.add(verb);
	}

	public List<ManyToMany> getManyToManies() {
		return manyToManies;
	}

	public void setManyToManies(List<ManyToMany> manyToManies) {
		this.manyToManies = manyToManies;
	}
	
	public void writeToFile(String filePath, String content) throws Exception {
		File f = new File(filePath);
		if (!f.getParentFile().exists()) {
			f.getParentFile().mkdirs();
		}
		f.createNewFile();
		try (Writer fw = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(f.getAbsolutePath()), "UTF-8"))) {
			fw.write(content, 0, content.length());
		}
	}
	
	protected Boolean setContainsDomain(Set<Domain> set, String domainName) {
		for (Domain d : set) {
			if (d.getStandardName().equals(domainName))
				return true;
		}
		return false;
	}

	protected Domain lookupDoaminInSet(Set<Domain> set, String domainName) {
		for (Domain d : set) {
			if (d.getStandardName().equals(domainName))
				return d;
		}
		return null;
	}
	public List<EasyUIPageTemplate> getEuTemplates() {
		return euTemplates;
	}
	
	public void setEuTemplates(List<EasyUIPageTemplate> euTemplates) {
		this.euTemplates = euTemplates;
	}
	
	public void addEuTemplate(EasyUIPageTemplate euTemplate) {
		this.euTemplates.add(euTemplate);
	}	
}
