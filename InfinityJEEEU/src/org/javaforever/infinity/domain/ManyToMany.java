package org.javaforever.infinity.domain;

import org.javaforever.infinity.complexverb.Assign;
import org.javaforever.infinity.complexverb.ListMyActive;
import org.javaforever.infinity.complexverb.ListMyAvailableActive;
import org.javaforever.infinity.complexverb.Revoke;
import org.javaforever.infinity.easyui.EasyUIManyToManyTemplate;
import org.javaforever.infinity.utils.StringUtil;

public class ManyToMany implements Comparable<ManyToMany>{
	protected Domain master;
	protected Domain slave;
	protected Assign assign;
	protected Revoke revoke;
	protected ListMyActive listMyActive;
	protected ListMyAvailableActive listMyAvailableActive;
	protected String standardName;
	protected String label;
	protected EasyUIManyToManyTemplate euTemplate;

	public ManyToMany(){
		super();
	}
	
	public ManyToMany(Domain master,Domain slave){
		this.master = master;
		this.slave = slave;
		this.assign = new Assign(master,slave);
		this.revoke = new Revoke(master,slave);
		this.listMyActive = new ListMyActive(master,slave);
		this.listMyAvailableActive = new ListMyAvailableActive(master,slave);
		this.euTemplate = new EasyUIManyToManyTemplate(master,slave); 
	}
	public void setMaster(Domain master) {
		this.master = master;
	}
	public Domain getSlave() {
		return slave;
	}
	public void setSlave(Domain slave) {
		this.slave = slave;
	}
	public Assign getAssign() {
		return assign;
	}
	public void setAssign(Assign assign) {
		this.assign = assign;
	}
	public Revoke getRevoke() {
		return revoke;
	}
	public void setRevoke(Revoke revoke) {
		this.revoke = revoke;
	}
	public ListMyActive getListMyActive() {
		return listMyActive;
	}
	public void setListMyActive(ListMyActive listMyActive) {
		this.listMyActive = listMyActive;
	}
	public ListMyAvailableActive getListMyAvailableActive() {
		return listMyAvailableActive;
	}
	public void setListMyAvailableActive(ListMyAvailableActive listMyAvailableActive) {
		this.listMyAvailableActive = listMyAvailableActive;
	}
	public Domain getMaster() {
		return master;
	}
	public String getStandardName(){
		return "Link"+this.master.getStandardName()+this.slave.getStandardName();
	}
	@Override
	public int compareTo(ManyToMany o) {
		return this.getStandardName().compareTo(o.getStandardName());
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	public String getText(){
		if (StringUtil.isBlank(this.master.getLabel())||StringUtil.isEnglishAndDigitalAndEmpty(this.master.getLabel())){
			return "Link "+this.master.getStandardName()+" "+this.slave.getStandardName();
		}
		else return "链接"+this.master.getText()+this.slave.getText();
	}
	
	public MysqlTwoDomainsDBDefinitionGenerator toTwoDBGenerator(){
		MysqlTwoDomainsDBDefinitionGenerator mtg = new MysqlTwoDomainsDBDefinitionGenerator(this.master,this.slave);
		return mtg;
	}

	public EasyUIManyToManyTemplate getEuTemplate() {
		return euTemplate;
	}

	public void setEuTemplate(EasyUIManyToManyTemplate euTemplate) {
		this.euTemplate = euTemplate;
	}
}
