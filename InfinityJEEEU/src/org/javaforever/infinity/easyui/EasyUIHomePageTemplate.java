package org.javaforever.infinity.easyui;

import java.util.ArrayList;
import java.util.List;

import org.javaforever.infinity.core.Writeable;
import org.javaforever.infinity.domain.Domain;
import org.javaforever.infinity.domain.DragonHideStatement;
import org.javaforever.infinity.domain.MenuItem;
import org.javaforever.infinity.domain.Statement;
import org.javaforever.infinity.domain.StatementList;
import org.javaforever.infinity.generator.JsonPagingGridJspTemplate;
import org.javaforever.infinity.utils.StringUtil;
import org.javaforever.infinity.utils.WriteableUtil;


public class EasyUIHomePageTemplate extends JsonPagingGridJspTemplate {
	protected String pageTitile = "Welcome to the System.";
	
	public EasyUIHomePageTemplate(){
		super();
	}
	
	public String generateJspString(){
		return generateStatementList().getContent();
	}
	
	@Override
	public StatementList generateStatementList() {
		try {
			List<Writeable> sList =  new ArrayList<Writeable>();
			sList.add(new Statement(1000L,0,"<!DOCTYPE html>"));
			sList.add(new Statement(2000L,0,"<html>"));
			sList.add(new Statement(3000L,0,"<head>"));
			sList.add(new Statement(4000L,0,"<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\" />"));
			sList.add(new Statement(5000L,0,"<title>无垠式代码生成器生成结果</title>"));
			sList.add(new Statement(6000L,0,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/themes/default/easyui.css\">"));
			sList.add(new Statement(7000L,0,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/themes/icon.css\">"));
			sList.add(new Statement(8000L,0,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/demo/demo.css\">"));
			sList.add(new Statement(9000L,0,"<script type=\"text/javascript\" src=\"../easyui/jquery.min.js\"></script>"));
			sList.add(new Statement(10000L,0,"<script type=\"text/javascript\" src=\"../easyui/jquery.easyui.min.js\"></script>"));
			sList.add(new Statement(11000L,0,"</head>"));
			sList.add(new Statement(12000L,0,"<body class=\"easyui-layout\">"));
			sList.add(new Statement(13000L,0,"<div data-options=\"region:'north',border:false\" style=\"height:60px;background:#B3DFDA;padding:10px\"><h2>无垠式代码生成器生成结果</h2></div>"));
			sList.add(new Statement(14000L,0,"<div data-options=\"region:'west',split:true,title:'主菜单'\" style=\"width:156px;padding:0px;\">"));
			sList.add(new Statement(15000L,0,"<div class=\"easyui-accordion\" data-options=\"fit:true,border:false\">"));
			sList.add(new Statement(16000L,0,"<div title=\"域对象清单\" style=\"padding:0px\" data-options=\"selected:true\">"));
			sList.add(new Statement(17000L,0,"<div id=\"mmadmin\" data-options=\"inline:true\" style=\"width: 142px; height: 98%; overflow: hidden; left: 0px; top: 0px; outline: none; display: block;\" class=\"menu-top menu-inline menu easyui-fluid\" tabindex=\"0\"><div class=\"menu-line\" style=\"height: 122px;\"></div>"));
			sList.add(new DragonHideStatement(17500L,0,"<div onclick=\"window.location='../pages/index.html'\" class=\"menu-item\" style=\"height: 20px;\"><div class=\"menu-text\" style=\"height: 20px; line-height: 20px;\">主页</div><div class=\"menu-icon icon-add\"></div></div>",!StringUtil.isBlank(this.allDomainList)&&this.allDomainList.size()>0&&!StringUtil.isBlank(this.allDomainList.get(0).getLabel())&&!StringUtil.isEnglishAndDigitalAndEmpty(this.allDomainList.get(0).getLabel())));
			sList.add(new DragonHideStatement(17500L,0,"<div onclick=\"window.location='../pages/index.html'\" class=\"menu-item\" style=\"height: 20px;\"><div class=\"menu-text\" style=\"height: 20px; line-height: 20px;\">Homepage</div><div class=\"menu-icon icon-add\"></div></div>",!(!StringUtil.isBlank(this.allDomainList)&&this.allDomainList.size()>0&&!StringUtil.isBlank(this.allDomainList.get(0).getLabel())&&!StringUtil.isEnglishAndDigitalAndEmpty(this.allDomainList.get(0).getLabel()))));
			long serial = 18000L;
			for (Domain d : this.allDomainList){
				sList.add(new Statement(serial,0,"<div onclick=\"window.location='../pages/"+d.getPlural().toLowerCase()+".html'\" class=\"menu-item\" style=\"height: 20px;\"><div class=\"menu-text\" style=\"height: 20px; line-height: 20px;\">"+d.getText()+"</div><div class=\"menu-icon icon-add\"></div></div>"));
				serial += 1000L;
			}
			for (MenuItem mi : this.menuItems){
				sList.add(new Statement(serial,0,"<div onclick=\"window.location='"+mi.getUrl()+"'\" class=\"menu-item\" style=\"height: 20px;\"><div class=\"menu-text\" style=\"height: 20px; line-height: 20px;\">"+mi.getText()+"</div><div class=\"menu-icon icon-add\"></div></div>"));
				serial += 1000L;
			}
			sList.add(new Statement(serial,0,"</div>"));
			sList.add(new Statement(serial+1000L,0,"</div>"));
			sList.add(new Statement(serial+2000L,0,"</div>"));
			sList.add(new Statement(serial+3000L,0,"</div>"));
			sList.add(new Statement(serial+4000L,0,"<div data-options=\"region:'east',split:true,collapsed:true,title:'属性'\" style=\"width:250px;overflow: hidden\">"));
			sList.add(new Statement(serial+5000L,0,"</div>"));
			sList.add(new Statement(serial+6000L,0,"<div data-options=\"region:'south',border:false\" style=\"height:50px;background:#A9FACD;padding:10px;text-align: center\">火箭船软件工作室版权所有。作者电邮:jerry_shen_sjf@qq.com QQ群:277689737</div>"));
			sList.add(new Statement(serial+7000L,0,"<div data-options=\"region:'center',title:'无垠式代码生成器生成结果主页'\">"));
			sList.add(new Statement(serial+7000L,0,"<h2>欢迎来的无垠式代码生成器世界！</h2>"));
			sList.add(new Statement(serial+8000L,0,"</div>"));
			sList.add(new Statement(serial+9000L,0,"</body>"));
			sList.add(new Statement(serial+10000L,0,"</html>"));
			StatementList mylist = WriteableUtil.merge(sList);
			return mylist;
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}

	public String getPageTitile() {
		return pageTitile;
	}

	public void setPageTitile(String pageTitile) {
		this.pageTitile = pageTitile;
	}
}
