package org.javaforever.infinity.easyui;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.javaforever.infinity.complexverb.Assign;
import org.javaforever.infinity.complexverb.ListMyActive;
import org.javaforever.infinity.complexverb.ListMyAvailableActive;
import org.javaforever.infinity.complexverb.Revoke;
import org.javaforever.infinity.complexverb.TwoDomainVerb;
import org.javaforever.infinity.core.Verb;
import org.javaforever.infinity.core.Writeable;
import org.javaforever.infinity.domain.Domain;
import org.javaforever.infinity.domain.DragonHideStatement;
import org.javaforever.infinity.domain.MenuItem;
import org.javaforever.infinity.domain.Statement;
import org.javaforever.infinity.domain.StatementList;
import org.javaforever.infinity.domain.Type;
import org.javaforever.infinity.domain.Var;
import org.javaforever.infinity.generator.NamedS2SMJavascriptMethodGenerator;
import org.javaforever.infinity.utils.StringUtil;
import org.javaforever.infinity.utils.WriteableUtil;
import org.javaforever.infinity.verb.ListActive;


public class EasyUIManyToManyTemplate {
	protected Domain master;
	protected Domain slave;
	protected String standardName;
	protected String label;
	protected Set<Verb> verbs = new TreeSet<Verb>();
	protected Set<TwoDomainVerb> twoDomainVerbs = new TreeSet<TwoDomainVerb>();
	protected List<Domain> menuDomainList = new ArrayList<Domain>();
	protected Set<MenuItem> menuItems = new TreeSet<MenuItem>();	

	public EasyUIManyToManyTemplate(Domain master,Domain slave){
		super();
		this.master = master;
		this.slave = slave;
		this.standardName = StringUtil.lowerFirst("link"+master.getCapFirstDomainName()+slave.getCapFirstDomainName());
		this.verbs.add(new ListActive(master));
		this.twoDomainVerbs.add(new ListMyActive(master,slave));
		this.twoDomainVerbs.add(new ListMyAvailableActive(master,slave));
		this.twoDomainVerbs.add(new Assign(master,slave));
		this.twoDomainVerbs.add(new Revoke(master,slave));
	}

	public String generateContentString(){
		return generateStatementList().getContent();
	}

	public StatementList generateStatementList() {
		try {
			List<Writeable> sList =  new ArrayList<Writeable>();
			sList.add(new Statement(1000L,0,"<!DOCTYPE html>"));
			sList.add(new Statement(2000L,0,"<html>"));
			sList.add(new Statement(3000L,0,"<head>"));
			sList.add(new Statement(4000L,0,"<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\" />"));
			sList.add(new Statement(5000L,0,"<title>"+this.getText()+"</title>"));
			sList.add(new Statement(6000L,0,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/themes/default/easyui.css\">"));
			sList.add(new Statement(7000L,0,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/themes/icon.css\">"));
			sList.add(new Statement(8000L,0,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/demo/demo.css\">"));
			sList.add(new Statement(9000L,0,"<script type=\"text/javascript\" src=\"../easyui/jquery.min.js\"></script>"));
			sList.add(new Statement(10000L,0,"<script type=\"text/javascript\" src=\"../easyui/jquery.easyui.min.js\"></script>"));
			sList.add(new Statement(11000L,0,"</head>"));
			sList.add(new Statement(12000L,0,"<body class=\"easyui-layout\">"));
			sList.add(new Statement(13000L,0,"<div data-options=\"region:'north',border:false\" style=\"height:60px;background:#B3DFDA;padding:10px\"><h2>无垠式代码生成器生成结果</h2></div>"));
			sList.add(new Statement(14000L,0,"<div data-options=\"region:'west',split:true,title:'主菜单'\" style=\"width:156px;padding:0px;\">"));
			sList.add(new Statement(15000L,0,"<div class=\"easyui-accordion\" data-options=\"fit:true,border:false\">"));
			sList.add(new Statement(16000L,0,"<div title=\"域对象清单\" style=\"padding:0px\" data-options=\"selected:true\">"));
			sList.add(new Statement(17000L,0,"<div id=\"mmadmin\" data-options=\"inline:true\" style=\"width: 142px; height: 98%; overflow: hidden; left: 0px; top: 0px; outline: none; display: block;\" class=\"menu-top menu-inline menu easyui-fluid\" tabindex=\"0\"><div class=\"menu-line\" style=\"height: 122px;\"></div>"));
			sList.add(new DragonHideStatement(17500L,0,"<div onclick=\"window.location='../pages/index.html'\" class=\"menu-item\" style=\"height: 20px;\"><div class=\"menu-text\" style=\"height: 20px; line-height: 20px;\">主页</div><div class=\"menu-icon icon-add\"></div></div>",!StringUtil.isBlank(this.getMenuDomainList())&&this.getMenuDomainList().size()>0&&!StringUtil.isBlank(this.getMenuDomainList().get(0).getLabel())&&!StringUtil.isEnglishAndDigitalAndEmpty(this.getMenuDomainList().get(0).getLabel())));
			sList.add(new DragonHideStatement(17500L,0,"<div onclick=\"window.location='../pages/index.html'\" class=\"menu-item\" style=\"height: 20px;\"><div class=\"menu-text\" style=\"height: 20px; line-height: 20px;\">Homepage</div><div class=\"menu-icon icon-add\"></div></div>",!(!StringUtil.isBlank(this.getMenuDomainList())&&this.getMenuDomainList().size()>0&&!StringUtil.isBlank(this.getMenuDomainList().get(0).getLabel())&&!StringUtil.isEnglishAndDigitalAndEmpty(this.getMenuDomainList().get(0).getLabel()))));
			long serial = 18000L;
			for (Domain d : this.menuDomainList){
				sList.add(new Statement(serial,0,"<div onclick=\"window.location='../pages/"+d.getPlural().toLowerCase()+".html'\" class=\"menu-item\" style=\"height: 20px;\"><div class=\"menu-text\" style=\"height: 20px; line-height: 20px;\">"+d.getText()+"</div><div class=\"menu-icon icon-add\"></div></div>"));
				serial += 1000L;
			}
			for (MenuItem mi : this.menuItems){
				sList.add(new Statement(serial,0,"<div onclick=\"window.location='"+mi.getUrl()+"'\" class=\"menu-item\" style=\"height: 20px;\"><div class=\"menu-text\" style=\"height: 20px; line-height: 20px;\">"+mi.getText()+"</div><div class=\"menu-icon icon-add\"></div></div>"));
				serial += 1000L;
			}
			sList.add(new Statement(serial,0,"</div>"));
			sList.add(new Statement(serial+1000L,0,"</div>"));
			sList.add(new Statement(serial+2000L,0,"</div>"));
			sList.add(new Statement(serial+3000L,0,"</div>"));
			sList.add(new Statement(serial+4000L,0,"<div data-options=\"region:'east',split:true,collapsed:true,title:'属性'\" style=\"width:250px;overflow: hidden\">"));
			sList.add(new Statement(serial+5000L,0,"<table class=\"easyui-datagrid\" title=\"Basic DataGrid\" style=\"width:250px;height:500px;overflow: hidden\">"));
			sList.add(new Statement(serial+7000L,0,"<thead>"));
			sList.add(new Statement(serial+8000L,0,"<tr>"));
			sList.add(new Statement(serial+9000L,0,"<th data-options=\"field:'id',width:50\">序号</th>"));
			sList.add(new Statement(serial+10000L,0,"<th data-options=\"field:'attr',width:100\">属性</th>"));
			sList.add(new Statement(serial+11000L,0,"<th data-options=\"field:'value',width:100\">值</th>"));
			sList.add(new Statement(serial+12000L,0,"</tr>"));
			sList.add(new Statement(serial+13000L,0,"</thead>"));
			sList.add(new Statement(serial+14000L,0,"</table>"));
			sList.add(new Statement(serial+15000L,0,"</div>"));
			sList.add(new Statement(serial+16000L,0,"<div data-options=\"region:'south',border:false\" style=\"height:50px;background:#A9FACD;padding:10px;text-align: center\">火箭船软件工作室版权所有。作者电邮:jerry_shen_sjf@qq.com QQ群:277689737</div>"));
			sList.add(new Statement(serial+17000L,0,"<div data-options=\"region:'center',title:'链接"+this.master.getText()+this.slave.getText()+"面板'\" style=\"height:450px;width:1100px;text-align:center\">"));
			sList.add(new Statement(serial+18000L,0,"<table>"));
			sList.add(new Statement(serial+19000L,0,"<tr>"));
			sList.add(new Statement(serial+20000L,0,"<td width=\"80px\">"));
			sList.add(new Statement(serial+21000L,0,"</td>"));
			sList.add(new Statement(serial+22000L,0,"<td>"));
			sList.add(new Statement(serial+23000L,0,"<div class=\"easyui-datalist\" title=\"活跃"+this.master.getText()+"\" id=\"my"+this.master.getCapFirstDomainName()+"\" style=\"width:200px;height:450px;float:left;display:inline;margin:0px\" data-options=\""));
			sList.add(new Statement(serial+24000L,0,"url: '../facade/listActive"+this.master.getCapFirstPlural()+"Facade',"));
			sList.add(new Statement(serial+25000L,0,"method: 'post',"));
			sList.add(new Statement(serial+26000L,0,"valueField:'"+this.master.getDomainId().getLowerFirstFieldName()+"',"));
			sList.add(new Statement(serial+27000L,0,"textField:'"+this.master.getDomainName().getLowerFirstFieldName()+"',"));
			sList.add(new Statement(serial+28000L,0,"idField:'"+this.master.getDomainId().getLowerFirstFieldName()+"',"));
			sList.add(new Statement(serial+29000L,0,"onCheck:checkMyRow,"));
			sList.add(new Statement(serial+30000L,0,"onLoadSuccess:loadSuccess"));
			sList.add(new Statement(serial+31000L,0,"\">"));
			sList.add(new Statement(serial+32000L,0,"</div>"));
			sList.add(new Statement(serial+33000L,0,"</td>"));
			sList.add(new Statement(serial+34000L,0,"<td>"));	
			sList.add(new Statement(serial+35000L,0,"<div class=\"easyui-datalist\" title=\"现有"+this.slave.getText()+"\" id=\"my"+this.slave.getCapFirstPlural()+"\" style=\"width:200px;height:450px;float:left;display:inline;margin:0px\" data-options=\""));
			ListMyActive listmac = new ListMyActive(this.master,this.slave);
			ListMyAvailableActive listavmac = new ListMyAvailableActive(this.master,this.slave);
			Assign assign = new Assign(this.master,this.slave);
			Revoke revoke = new Revoke(this.master,this.slave);
			sList.add(new Statement(serial+36000L,0,"url: '../facade/"+StringUtil.lowerFirst(listmac.getVerbName())+"Facade',"));
			sList.add(new Statement(serial+37000L,0,"method: 'post',"));
			sList.add(new Statement(serial+38000L,0,"valueField:'"+this.slave.getDomainId().getLowerFirstFieldName()+"',"));
			sList.add(new Statement(serial+39000L,0,"textField:'"+this.slave.getDomainName().getLowerFirstFieldName()+"',"));
			sList.add(new Statement(serial+40000L,0,"singleSelect:false"));
			sList.add(new Statement(serial+41000L,0,"\">"));
			sList.add(new Statement(serial+42000L,0,"</div>"));
			sList.add(new Statement(serial+43000L,0,"</td>"));
			sList.add(new Statement(serial+44000L,0,"<td>"));
			sList.add(new Statement(serial+45000L,0,"<div style=\"float:left;display:inline-block\">"));
			sList.add(new Statement(serial+46000L,0,"<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>"));
			sList.add(new Statement(serial+47000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\""+StringUtil.lowerFirst(assign.getVerbName())+"()\">&lt;&lt;</a><br/><br/><br/>"));
			sList.add(new Statement(serial+48000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\""+StringUtil.lowerFirst(revoke.getVerbName())+"()\">&gt;&gt;</a>"));
			sList.add(new Statement(serial+49000L,0,"</div>"));
			sList.add(new Statement(serial+50000L,0,"<div class=\"easyui-datalist\" title=\"可添加"+this.slave.getText()+"\" id=\"myAvailable"+this.slave.getCapFirstPlural()+"\" style=\"width:200px;height:450px;float:left;display:inline;margin:0px\" data-options=\""));
			sList.add(new Statement(serial+51000L,0,"url: '../facade/"+StringUtil.lowerFirst(listavmac.getVerbName())+"Facade',"));
			sList.add(new Statement(serial+52000L,0,"method: 'post',"));
			sList.add(new Statement(serial+53000L,0,"valueField:'"+this.slave.getDomainId().getLowerFirstFieldName()+"',"));
			sList.add(new Statement(serial+54000L,0,"textField:'"+this.slave.getDomainName().getLowerFirstFieldName()+"',"));
			sList.add(new Statement(serial+55000L,0,"singleSelect:false"));
			sList.add(new Statement(serial+56000L,0,"\">"));
			sList.add(new Statement(serial+57000L,0,"</div>"));
			sList.add(new Statement(serial+58000L,0,"</td>"));
			sList.add(new Statement(serial+59000L,0,"</tr>"));
			sList.add(new Statement(serial+60000L,0,"</table>"));
			sList.add(new Statement(serial+61000L,0,"</div>"));		
			sList.add(new Statement(serial+62000L,0,"</body>"));
			sList.add(new Statement(serial+63000L,0,"<script type=\"text/javascript\">"));
			sList.add(new Statement(serial+63500L,0,"var currentIndex = 0;"));
			Var currentIndex = new Var("currentIndex", new Type("var"));
			sList.add(NamedS2SMJavascriptMethodGenerator.generateCheckMyRowMethod(this.master,this.slave,currentIndex).generateMethodStatementListIncludingContainer(serial+64000L,0));
			sList.add(NamedS2SMJavascriptMethodGenerator.generateLoadSuccessMethod(this.master,currentIndex).generateMethodStatementListIncludingContainer(serial+65000L,0));
			
			sList.add(assign.generateEasyUIJSActionMethod().generateMethodStatementListIncludingContainer(serial+66000L,0));
			sList.add(revoke.generateEasyUIJSActionMethod().generateMethodStatementListIncludingContainer(serial+67000L,0));
			sList.add(NamedS2SMJavascriptMethodGenerator.generateIsBlankMethod().generateMethodStatementListIncludingContainer(serial+68000L,0));
			sList.add(new Statement(serial+69000L,0,"</script>"));
			sList.add(new Statement(serial+70000L,0,"</html>"));
			StatementList mylist = WriteableUtil.merge(sList);
			return mylist;
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}

	public List<Domain> getMenuDomainList() {
		return menuDomainList;
	}

	public void setMenuDomainList(List<Domain> menuDomainList) {
		this.menuDomainList = menuDomainList;
	}

	public Domain getMaster() {
		return master;
	}

	public void setMaster(Domain master) {
		this.master = master;
	}

	public Domain getSlave() {
		return slave;
	}

	public void setSlave(Domain slave) {
		this.slave = slave;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Set<Verb> getVerbs() {
		return verbs;
	}

	public void setVerbs(Set<Verb> verbs) {
		this.verbs = verbs;
	}

	public Set<TwoDomainVerb> getTwoDomainVerbs() {
		return twoDomainVerbs;
	}

	public void setTwoDomainVerbs(Set<TwoDomainVerb> twoDomainVerbs) {
		this.twoDomainVerbs = twoDomainVerbs;
	}
	
	public String getText(){
		if (this.label!= null && !this.label.equals("")) return this.label;
		else return this.getStandardName();
	}

	public Set<MenuItem> getMenuItems() {
		return menuItems;
	}

	public void setMenuItems(Set<MenuItem> menuItems) {
		this.menuItems = menuItems;
	}
}
